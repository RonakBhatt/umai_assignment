class ChangeDateFormatInMyTable < ActiveRecord::Migration[5.2]
  def up
    change_column :restaurants, :open_time, :datetime
    change_column :restaurants, :exit_time, :datetime
  end

  def down
    change_column :restaurants, :open_time, :time
    change_column :restaurants, :exit_time, :time
  end
end
