Rails.application.routes.draw do
	root :to => "home#index"
  get 'home/index'
  ActiveAdmin.routes(self)
  # api_version(:module => "V1", :path => {:value => "v1"}) do
  # end  
  # constraints subdomain: 'api' do
  # end
  api_version(:module => "V1", :path => {:value => "v1"}) do
	 resources :guests
   resources :restaurants
   resources :tables
   resources :reservations
	end
end