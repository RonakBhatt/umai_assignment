ActiveAdmin.register Guest do

	permit_params :name, :email
	
  form :html => { :enctype => "multipart/form-data" } do |f|  
    f.inputs "Guest" do
	    f.input :name
	    f.input :email, :as => :email
	    f.actions
  	end
	end

	index do
		selectable_column
	  column :name
	  column :email
	  actions
	end

	show do
    attributes_table do
      row :name
      row :email
    end
  end

end
