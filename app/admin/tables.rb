	ActiveAdmin.register Table do
	# See permitted parameters documentation:
	# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
	#
	permit_params :name, :capacity, :restaurant_id

	# form :html => { :enctype => "multipart/form-data" } do |f|  
	#   f.inputs "table" do
	#     f.input :name
	#     f.input :capacity
	#     f.actions
	# 	end
	# end

	index do
		selectable_column
	  column :name
	  column :capacity
	  column :restaurant
	  actions
	end

	show do
	  attributes_table do
	    row :name
	    row :capacity
	    row :restaurant
	  end
	end

end
