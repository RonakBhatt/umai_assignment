ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    # div class: "blank_slate_container", id: "dashboard_default_message" do
    #   span class: "blank_slate" do
    #     span I18n.t("active_admin.dashboard_welcome.welcome")
    #     small I18n.t("active_admin.dashboard_welcome.call_to_action")
    #   end
    # end

    # Here is an example of a simple dashboard with columns and panels.
    #
    columns do
      column do
        panel "Recent Restaurant" do
          ul do
            Restaurant.all.map do |restaurant|
              li link_to(restaurant.name, admin_restaurant_path(restaurant))
            end
          end
        end
      end

      column do
        panel "Recent Guest" do
          ul do
            Guest.all.map do |guest|
              li link_to(guest.name, admin_guest_path(guest))
            end
          end
        end
      end

      column do
        panel "Recent Tables" do
          ul do
            Table.all.map do |table|
              li link_to(table.name, admin_table_path(table))
            end
          end
        end
      end
    end
  end # content
end
