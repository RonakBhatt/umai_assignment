ActiveAdmin.register Restaurant do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
	permit_params :name, :email, :phone, :open_time, :exit_time
	
  form :html => { :enctype => "multipart/form-data" } do |f|  
    f.inputs "restaurant" do
	    f.input :name
	    f.input :email, :as => :email
	    f.input :phone
			f.input :open_time, :as => :time_picker
	    f.input :exit_time, :as => :time_picker
	    f.actions
  	end
	end

	index do
		selectable_column
	  column :name
	  column :email
	  column :phone
	  column :open_time, :sortable => :open_time do |r|
	    r.open_time.strftime('%H:%M - %p')
	  end
	  column :exit_time, :sortable => :exit_time do |r|
	    r.exit_time.strftime('%H:%M - %p')
	  end
	  actions
	end

	show do
    attributes_table do
      row :name
      row :email
      row :phone
      row :open_time
      row :exit_time
    end
  end
end
