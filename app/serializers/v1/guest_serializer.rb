class GuestSerializer < ActiveModel::Serializer
  attributes :name, :email
end