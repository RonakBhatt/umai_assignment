require 'json'
class V1::TablesController < ApplicationController
  def index
    tables = Table.all
    render json: { tables: tables }, status: :ok
  end

  def create
	  table = Table.new(name: params[:name], capacity: params[:capacity], restaurant_id: params[:restaurant_id])
	  if table.save
	    render json: { table: table }, status: :created
	  else
	    render json: { errors: table.errors }, status:    :unprocessable_entity
	  end
	end
end