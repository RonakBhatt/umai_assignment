class V1::ReservationsController < ApplicationController
  def index
    reservations = Reservation.all
    render json: { reservations: reservations }, status: :ok
  end

  def create
  	if params[:restaurant_id].present?
  		restaurant_id = params[:restaurant_id]
  		restaurant = Restaurant.find_by_id(restaurant_id.to_i)
  	end
  	if restaurant.tables.present?
		  reservation = Reservation.new(guest_id: params[:guest_id], restaurant_id: restaurant_id)
		  if reservation.save
		    render json: { reservation: reservation }, status: :created
		  else
		    render json: { errors: reservation.errors }, status:    :unprocessable_entity
		  end
		else
			render json: { errors: "no tables Available for this restaurant" }, status:    :unprocessable_entity
		end
	end

	private

    def reservation_params
        params.require(:reservation).permit(:guest_id, :table_id)
    end
end