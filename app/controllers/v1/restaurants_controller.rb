require 'json'
class V1::RestaurantsController < ApplicationController
  def index
    restaurants = Restaurant.all
    render json: { restaurants: restaurants }, status: :ok
  end

  def create
	  restaurant = Restaurant.new(name: params[:name], email: params[:email], phone: params[:phone], open_time: params[:open_time].to_time, exit_time: params[:exit_time].to_time)
	  if restaurant.save
	    render json: { restaurant: restaurant }, status: :created
	  else
	    render json: { errors: restaurant.errors }, status:    :unprocessable_entity
	  end
	end
end

