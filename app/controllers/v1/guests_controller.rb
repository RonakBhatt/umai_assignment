class V1::GuestsController < ApplicationController
  def index
    guests = Guest.all
    render json: { guests: guests }, status: :ok
  end

  def create
	  guest = Guest.new(name: params[:name], email: params[:email])
	  if guest.save
	    render json: { guest: guest }, status: :created
	  else
	    render json: { errors: guest.errors }, status:    :unprocessable_entity
	  end
	end
end