class Reservation < ApplicationRecord
  belongs_to :guest
  belongs_to :restaurant
  belongs_to :table, optional: true
end
