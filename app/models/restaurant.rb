class Restaurant < ApplicationRecord
	validates :phone, numericality: {only_integer: true}
	validates :email, format: { with: URI::MailTo::EMAIL_REGEXP } 
	validates :name, presence: true
	validates :email, presence: true
	validates :phone, presence: true
	validates :open_time, presence: true
	validates :exit_time, presence: true
	has_many :tables
    has_many :reservations, through: :tables
end
