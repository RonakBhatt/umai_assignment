class Table < ApplicationRecord
  belongs_to :restaurant
  validates :name, presence: true
  validates :capacity, presence: true
	has_many :reservations
	has_many :guests, through: :reservations
end
