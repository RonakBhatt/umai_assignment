class Guest < ApplicationRecord
	validates :name, presence: true
	validates :email, presence: true
	validates :email, format: { with: URI::MailTo::EMAIL_REGEXP } 
	has_many :reservations
    has_many :tables, through: :reservations
end
